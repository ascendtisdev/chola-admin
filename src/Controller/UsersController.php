<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Home
 * @package   Home
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Admins Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Admins
 * @package  Admins
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class UsersController extends AppController
{
  public $components = ['Query','Paginator'];

  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * View Users
   * 
   * @return void
   */
  public function view() {
    $this->viewBuilder()->setLayout('base_layout');

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 20,
      'conditions' => [
        'Users.is_active' => 1
      ],
      'order' => [
        'Users.name' => 'asc'
      ],
      'contain' => [
        'UserSubscriptions',
        'Runners'
      ]
    ];
  
    $details=$this->Users->find('all');
    $subscriptionList = $this->Query->getAllDataList('Subscriptions', 
      [
        'Subscriptions.is_active' => 1
      ]
    );
    $this->set('data', $this->paginate($details));
    $this->set('subscriptions', $subscriptionList);
  }

  /**
   * Add User
   * 
   * @return void
   */
  public function add() {
    $this->viewBuilder()->setLayout('base_layout');
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      $checkUsername = $this->Users->find()->where(['Users.username'=>$data['username']]);
      if ($checkUsername->count() > 0) {
        $this->Flash->error('Oops! Username is already exists.');
        return $this -> redirect(array('controller' => 'users', 'action' => 'add'));
      }

      $data['is_active'] = 1;
      //Add User
      if ($this->Query->setData('Users', $data)) {
        $this->Flash->success('User `'.$data['username'].'` has been added.');
        return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'users', 'action' => 'add'));
      }

    }
    $this->set('page_title', 'Add User');
  }

  /**
   * Edit User
   * 
   * @return void
   */
  public function edit($id = null) {
    $this->viewBuilder()->setLayout('base_layout');

    $data = $this->Query->getAllDataById('Users', [ 'Users.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! User not found.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    }

    if ($this->request->is('post')) {
      $data = $this->request->getData();
      
      $data['id'] = $id;
      //Add User
      if ($this->Query->setData('Users', $data)) {
        $this->Flash->success('User `'.$data['name'].'` has been updated.');
        return $this -> redirect(array('controller' => 'users', 'action' => 'edit', $id));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'users', 'action' => 'edit', $id));
      }
      
    }
  }

  /**
   * Delete User
   * 
   * @return void
   */
  public function delete($id = null) {

    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    }

    $data = $this->Query->getAllDataById('Users', [ 'Users.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! User not found.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    }

    $tmpData['id'] = $id;
    $tmpData['is_active'] = 0; //Soft Delete
    //Add User
    if ($this->Query->setData('Users', $tmpData)) {
      $this->Flash->success('User `'.$data['name'].'` has been deleted.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    }
    
  }
}
?>