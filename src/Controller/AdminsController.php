<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Home
 * @package   Home
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Admins Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Admins
 * @package  Admins
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class AdminsController extends AppController
{
  public $components = ['Query','Paginator'];

  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow(['register']);
  }

  /**
   * Login Page
   *
   * Used for Login View.
   *
   * @return void
   */
  public function login() {
    $this->viewBuilder()->setLayout("login");

    if ($this->request->is('post')) {
      $admin = $this->Auth->identify();

      if ($admin) {
        $this->Auth->setUser($admin);
        $this->Flash->success('LoggedIn Successfully!');
        return $this->redirect($this->Auth->redirectUrl());
      } else {
        $this->Flash->error('Your username or password is incorrect.');
      }
    }
  }

  /**
   * Dashboard
   * 
   * @return void
   */
  public function dashboard() {
    $this->viewBuilder()->setLayout('base_layout'); //base_layout

    $admins = $this->Admins->find();

    $this->set('admins', $admins->count());
  }

  /**
   * Add Admin
   * 
   * @return void
   */
  public function add() {
    $this->viewBuilder()->setLayout('base_layout');

    if ($this->request->is('post')) {
      $data = $this->request->getData();

      if($data['password'] != $data['rpassword']) {
        $this->Flash->error('Your password and retype password is not matched.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'add'));
      }

      $checkUsername = $this->Admins->find()->where(['Admins.username'=>$data['username']]);
      if ($checkUsername->count() > 0) {
        $this->Flash->error('Oops! Username is already exists.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'add'));
      }

      unset($data['rpassword']);
      //Add Admin
      if ($this->Query->setData('Admins', $data)) {
        $this->Flash->success('Admin `'.$data['username'].'` has been added.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'add'));
      }

    }

    $this->set('page_title', 'Add Admin');
  }

  /**
   * Edit Admins
   * 
   * @return void
   */
  public function edit($id = null) {
    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
    }
    $this->viewBuilder()->setLayout('base_layout');
    $data = $this->Query->getAllDataById('Admins', [ 'Admins.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Admin not found.');
      return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
    }

    if ($this->request->is('post')) {
      $data = $this->request->getData();
      if(trim($data['password']) != trim($data['rpassword'])) {
        $this->Flash->error('Oops! Both password must be same or just leave it blank.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'edit', $id));
      }

      $data['id'] = $id;
      unset($data['rpassword']);
      if(trim($data['password']) < 2  || trim($data['password']) === '') {
        unset($data['password']);
      }

      if ($this->Query->setData('Admins',  $data)) {
        $this->Flash->success('Admin `'.$data['username'].'` has been edited.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'edit', $id));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'edit', $id));  
      }

    }

    $this->set('page_title', 'Edit Admin');
  }

  /**
   * Delete Admin
   * 
   * @return void
   */
  public function delete($id = null) {
    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
    }
    $data = $this->Query->getAllDataById('Admins', [ 'Admins.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Admin not found.');
      return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
    }

    if ($this->Query->removeData('Admins',['Admins.id' => $id])) {
      $this->Flash->success('The Admin has been deleted.');
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
    }
    return $this -> redirect(array('controller' => 'admins', 'action' => 'view'));
  }

  /**
   * View Admins
   * 
   * @return void
   */
  public function view() {
    $this->viewBuilder()->setLayout('base_layout');

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 20,
      'order' => [
        'Admins.name' => 'asc'
      ]
    ];
  
    $details=$this->Admins->find('all');
    $this->set('data', $this->paginate($details));
  }


  /** 
   * Change Password
   * 
   * @return void
   */
  public function changePassword() {
    $this->viewBuilder()->setLayout('base_layout');

    $user = $this->Auth->user();
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      if(trim($data['password']) != trim($data['rpassword'])) {
        $this->Flash->error('Oops! Both password must be same.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'change_password'));
      }

      unset($data['rpassword']);
      $data['id'] = $user['id'];
      if ($this->Query->setData('Admins',  $data)) {
        $this->Flash->success('Password has been changed successfully.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'dashboard'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'admins', 'action' => 'dashboard'));  
      }

    }

    $this->set('page_title','Change Password');
  }

  // /**
  //  * Register Page
  //  *
  //  * Used for Register View.
  //  *
  //  * @return void
  //  */
  // public function register() {
  //   $this->viewBuilder()->setLayout("login");

  //   if ($this->request->is('post')) {
  //     $data = $this->request->getData();
      
  //     $this->Query->setData('Admins', $data);
  //   }
  // }

  /**
   * Logout
   * @return redirect
   */
  public function logout() {
    return $this->redirect($this->Auth->logout());
  }


  /**
   * 
   */
}

?>