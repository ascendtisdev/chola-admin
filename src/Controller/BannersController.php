<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Banners
 * @package   Banners
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Banners Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Banners
 * @package  Banners
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class BannersController extends AppController
{
  public $components = ['Query','Paginator'];
  
  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * View Banners
   * 
   * @return void
   */
  public function view() {
    $this->viewBuilder()->setLayout('base_layout');

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 20,
      'conditions' => [
        'Banners.is_active' => 1
      ],
      'order' => [
        'Banners.precedence' => 'asc'
      ]
    ];
  
    $details=$this->Banners->find('all');
    $this->set('data', $this->paginate($details));
  }

  /**
   * Add Banner
   * 
   * @return void
   */
  public function add() {
    $this->viewBuilder()->setLayout('base_layout');
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      $data['is_active'] = 1;
      //Add User
      if ($this->Query->setData('Banners', $data)) {
        $this->Flash->success('Banner `'.$data['title'].'` has been added.');
        return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'banners', 'action' => 'add'));
      }

    }
    $this->set('page_title', 'Add Banner');
  }

  /**
   * Edit Banner
   * 
   * @return void
   */
  public function edit($id = null) {
    $this->viewBuilder()->setLayout('base_layout');

    $data = $this->Query->getAllDataById('Banners', [ 'Banners.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Banner not found.');
      return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
    }

    if ($this->request->is('post')) {
      $data = $this->request->getData();
      
      $data['id'] = $id;

      // Ignore Image if it is not selected
      if (empty($data['photo']['tmp_name']))  {
        unset($data['photo']);
      }
      //-=--=-=-=-=
      
      //Edit Subscription
      if ($this->Query->setData('Banners', $data)) {
        $this->Flash->success('Banner `'.$data['title'].'` has been updated.');
        return $this -> redirect(array('controller' => 'banners', 'action' => 'edit', $id));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'banners', 'action' => 'edit', $id));
      }
      
    }
  }

  /**
   * Delete Banner
   * 
   * @return void
   */
  public function delete($id = null) {

    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
    }

    $data = $this->Query->getAllDataById('Banners', [ 'Banners.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Banner not found.');
      return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
    }

    $tmpData['id'] = $id;
    $tmpData['is_active'] = 0; //Soft Delete

    if ($this->Query->setData('Banners', $tmpData)) {
      $this->Flash->success('Banner `'.$data['title'].'` has been deleted.');
      return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
      return $this -> redirect(array('controller' => 'banners', 'action' => 'view'));
    }
    
  }
}
?>