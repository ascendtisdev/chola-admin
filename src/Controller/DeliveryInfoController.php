<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  DeliveryInfo
 * @package   DeliveryInfo
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeliveryInfo Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category DeliveryInfo
 * @package  DeliveryInfo
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class DeliveryInfoController extends AppController
{
  public $components = ['Query','Paginator','UserWallet'];
  
  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  public function index() {
    $this->viewBuilder()->setLayout('base_layout');
    
    $statusFilter = $this->request->query('status');
    $startDateFilter = $this->request->query('start_date');
    $endDateFilter = $this->request->query('end_date');

    $data = $this->request->getData();

    $status = ['PENDING','DELIVERED','NOT_DELIVERED','NOT_AVAILABLE'];
    $selectedStartDate = date('Y-m-d');
    $selectedEndDate = date('Y-m-d');
    if ($statusFilter != 'ALL' && ($statusFilter != null || !empty($statusFilter))) {
      $status = [$statusFilter];
    }
    if ($statusFilter == null || empty($statusFilter)) {
      $statusFilter = 'ALL';
    }

    if ($startDateFilter != null || !empty($startDateFilter)) {
      $selectedStartDate = $startDateFilter;
    }
    if ($endDateFilter != null || !empty($endDateFilter)) {
      $selectedEndDate = $endDateFilter;
    }

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 30,
      'conditions' => [
        'DeliveryInfo.status IN' => $status,
        'DeliveryInfo.date >=' => $selectedStartDate,
        'DeliveryInfo.date <=' => $selectedEndDate
      ],
      'order' => [
        'DeliveryInfo.date' => 'asc'
      ],
      'contain' => [
        'Users',
        'Runners.Users'
      ]
    ];
    $details=$this->DeliveryInfo->find('all');
    $this->set('data', $this->paginate($details));

    //Get Runners List
    $runnersList = $this->getRunnersList();
    $this->set('runners_list', $runnersList);

    $this->set('filter_start_date', $selectedStartDate);
    $this->set('filter_end_date', $selectedEndDate);
    $this->set('filter_status', $statusFilter);

    $this->set('page_title', 'Delivery Information');
  }

  public function assign() {
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      if (sizeof($data['check']) > 0) {
        $tmp = [];
        $tmp['runner_id'] = $data['assign_to'];
        $this->Query->updateData('DeliveryInfo', $tmp, ['DeliveryInfo.id IN' => $data['check']]);

        $this->Flash->success('Delivery Boy successfully assigned.');
        return $this -> redirect($this->referer());
      } else {
        $this->Flash->error('Oops! Please select at least one delivery.');
        return $this -> redirect($this->referer());
      }
    }
  }

  private function getRunnersList() {
    $runners = $this->Query->getAllData('Runners',
      [
        'Runners.is_active' => 1
      ],
      [],
      false,
      ['Users']
    );

    $data = [];
    $this->log($runners);
    foreach ($runners as $key=>$val) {
      $data[$val['id']] = $val['user']['name'];
    }

    $this->log($data);
    return $data;
  }

  /**
   * Edit Delivery Info
   * 
   * @return void
   */
  public function edit() {
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      if (isset($data['start_date'])) {
        unset($data['start_date']);
      }

      $oldData = $this->Query->getAllDataById('DeliveryInfo',
        ['DeliveryInfo.id' => $data['id']],
        [],
        ['UserSubscriptions','Users']
      );
      
      if ($resp = $this->Query->setData('DeliveryInfo', $data)) {
        $this->Flash->success('Delivery Schedule has been updated.');

        if ($oldData['status'] != $data['status'] && $data['status'] === 'DELIVERED') {
          if (!$this->isDeliveryEntryExists($oldData['user_id'], $oldData['date'])) {
            //if false it means entry does not exists. - Then only insert.
            //Add Empty Bottles to User.
            $tmpData = [];
            $tmpData['id'] = $oldData['user_id'];
            $tmpData['empty_bottles'] = ($oldData['user']['empty_bottles'] + $oldData['user_subscription']['subscription_per_day_bottles_to_deliver']);
            $this->Query->setData('Users', $tmpData);
            //---=-=-=-=-=
            
            $this->UserWallet->giveUserPoints($oldData['user_id'], -$oldData['user_subscription']['subscription_per_day_price'], 'Milk Delivered on '.$oldData['date'], null, $oldData['date']);
          }
        }

        return $this -> redirect(array('controller' => 'delivery_info', 'action' => 'index'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'delivery_info', 'action' => 'index'));
      }

    }
  }

  /**
   * Check in UserWallet, is entry existed for the day(delivery_date) of delivery or not
   * 
   * @return boolean
   */
  private function isDeliveryEntryExists($user_id = null, $date = null) {
    if ($date == null) {
      return false;
    }

    $userWallet = $this->Query->getDataById('UserWallets', 
      [
        'UserWallets.delivery_date' => $date,
        'UserWallets.user_id' => $user_id
      ]
    );

    if (isset($userWallet['id']) && $userWallet['id'] != null) {
      return true;
    } else {
      return false;
    }
  }
  
}
?>