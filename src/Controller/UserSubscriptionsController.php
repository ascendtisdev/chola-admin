<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  UserSubscriptions
 * @package   UserSubscriptions
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserSubscriptions Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category UserSubscriptions
 * @package  UserSubscriptions
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class UserSubscriptionsController extends AppController
{
  public $components = ['Query','Paginator','UserWallet'];
  
  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * Add Subscription
   * 
   * @return void
   */
  public function add() {
    if ($this->request->is('post')) {
      $data = $this->request->getData();
      
      $data['days'] = $this->_getDaysBetweenTwoDates($data['start_date'], $data['end_date']);

      $subscriptionDetails = $this->_getSubscription($data['subscription_id']);
      $data['subscription_name'] = $subscriptionDetails['name'];
      $data['subscription_price'] = $subscriptionDetails['price'];
      $data['subscription_per_day_price'] = $subscriptionDetails['per_day_price'];
      $data['subscription_per_day_bottles_to_deliver'] = $subscriptionDetails['per_day_bottles_to_deliver'];
      $data['qty'] =  $subscriptionDetails['qty'];
      $data['is_active'] = 1;

      if ($resp = $this->Query->setData('UserSubscriptions', $data)) {
        
        $deliveryInfo = $this->_addDeliveryInfo($resp);
        if ($this->Query->setAllData('DeliveryInfo', $deliveryInfo)) {
          $this->UserWallet->giveUserPoints($data['user_id'], $data['subscription_price'],'Subscription `'.$data['subscription_name'].'` added.');
          $this->Flash->success('Subscription successfully activated.');
          return $this -> redirect(array('controller' => 'users', 'action' => 'view'));  
        }
      }
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
      return $this -> redirect(array('controller' => 'users', 'action' => 'view'));
    }
  }

  /**
   * Get Subscription Details
   * 
   * @return array
   */
  private function _getSubscription($subscription_id = null) {
    if ($subscription_id != null) {
      $data = $this->Query->getAllDataById('Subscriptions',
        [
          'Subscriptions.id' => $subscription_id
        ]
      );
      return $data;
    }
    return false;
  }


  /**
   * Get Days Between 2 Dates
   * 
   * @return integer
   */
  private function _getDaysBetweenTwoDates($start_date = null, $end_date = null) {
    $start_date = new \DateTime($start_date);
    $end_date = new \DateTime($end_date);
    $interval = $start_date->diff($end_date);
    return $interval->days;
  }

  /**
   * Creating date collection between two dates
   *
   * <code>
   * <?php
   * # Example 1
   * date_range("2014-01-01", "2014-01-20", "+1 day", "m/d/Y");
   *
   * # Example 2. you can use even time
   * date_range("01:00:00", "23:00:00", "+1 hour", "H:i:s");
   * </code>
   *
   * @author Ali OYGUR <alioygur@gmail.com>
   * @param string since any date, time or datetime format
   * @param string until any date, time or datetime format
   * @param string step
   * @param string date of output format
   * @return array
   */
  private function _date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
  }


  /**
   * Add Delivery Info for Subscription
   * 
   * @return boolean
   */
  private function _addDeliveryInfo($user_subscription = []) {
    if (isset($user_subscription['id'])) {
      $dates = $this->_date_range($user_subscription['start_date'], $user_subscription['end_date']);
      $tmpData = [];

      foreach ($dates as $key => $val) {
        $tmp = [];
        $tmp['user_id'] = $user_subscription['user_id'];
        $tmp['date'] = $val;
        $tmp['status'] =  'PENDING';
        $tmp['user_subscription_id'] = $user_subscription['id'];
        $tmp['qty'] = $user_subscription['qty'];
        $tmp['delivery_slot'] = $user_subscription['delivery_slot'];
        array_push($tmpData, $tmp);
      }

      return $tmpData;
    }
    return false;
  }
}
?>