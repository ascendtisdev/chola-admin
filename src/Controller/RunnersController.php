<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Runners
 * @package   Runners
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Runners Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Runners
 * @package  Runners
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class RunnersController extends AppController
{
  public $components = ['Query','Paginator'];
  
  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * View Runners
   * 
   * @return void
   */
  public function view() {
    $this->viewBuilder()->setLayout('base_layout');

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 20,
      'conditions' => [
        'Runners.is_active' => 1
      ],
      'order' => [
        'Runners.name' => 'asc'
      ],
      'contain' => [
        'Users'
      ]
    ];
  
    $details=$this->Runners->find('all');
    $this->set('data', $this->paginate($details));
  }

  /**
   * Add Runner
   * 
   * @return void
   */
  // public function add() {
  //   $this->viewBuilder()->setLayout('base_layout');
  //   if ($this->request->is('post')) {
  //     $data = $this->request->getData();

  //     $checkUsername = $this->Runners->find()->where(['Runners.username'=>$data['username']]);
  //     if ($checkUsername->count() > 0) {
  //       $this->Flash->error('Oops! Username is already exists.');
  //       return $this -> redirect(array('controller' => 'runners', 'action' => 'add'));
  //     }

  //     $data['is_active'] = 1;
  //     //Add User
  //     if ($this->Query->setData('Runners', $data)) {
  //       $this->Flash->success('Runner `'.$data['username'].'` has been added.');
  //       return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //     } else {
  //       $this->Flash->error('Oops! Something went wrong. Please try again later.');
  //       return $this -> redirect(array('controller' => 'runners', 'action' => 'add'));
  //     }

  //   }
  //   $this->set('page_title', 'Add Runner');
  // }


  /**
   * Add Runner
   * 
   * @return void
   */
  public function add($user_id = null) {

    $checkUser = $this->Runners->find()->where(['Runners.user_id'=>$user_id]);


      if ($checkUser->count() > 0) {
        $this->Flash->error('Oops! User is already runner.');
        return $this -> redirect(array('controller' => 'runners', 'action' => 'add'));
      }
      $data = [];
      $data['user_id'] = $user_id;
      $data['is_active'] = 1;
      //Add User
      if ($this->Query->setData('Runners', $data)) {
        $this->Flash->success('Runner has been added.');
        return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this -> redirect(array('controller' => 'runners', 'action' => 'add'));
      }
  }
  
  /**
   * Edit Runner
   * 
   * @return void
   */
  // public function edit($id = null) {
  //   $this->viewBuilder()->setLayout('base_layout');

  //   $data = $this->Query->getAllDataById('Runners', [ 'Runners.id' => $id ]);
  //   if (isset($data['id'])) {
  //     $this->set('data', $data);
  //   } else {
  //     $this->Flash->error('Oops! Runner not found.');
  //     return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //   }

  //   if ($this->request->is('post')) {
  //     $data = $this->request->getData();
      
  //     $data['id'] = $id;
  //     //Add User
  //     if ($this->Query->setData('Runners', $data)) {
  //       $this->Flash->success('Runner `'.$data['name'].'` has been updated.');
  //       return $this -> redirect(array('controller' => 'runners', 'action' => 'edit', $id));
  //     } else {
  //       $this->Flash->error('Oops! Something went wrong. Please try again later.');
  //       return $this -> redirect(array('controller' => 'runners', 'action' => 'edit', $id));
  //     }
      
  //   }
  // }

  /**
   * (HardDelete) Delete Runner Linking
   * 
   * @return void
   */
  public function delete($id = null) {
    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
    }

    $data = $this->Query->getAllDataById('Runners', [ 'Runners.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Runner not found.');
      return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
    }

    if ($this->Query->removeData('Runners',['Runners.id' => $id])) {
      $this->Flash->success('The Admin has been deleted.');
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
    }
    return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  }

  /**
   * (SoftDelete) Delete Runner
   * 
   * @return void
   */
  // public function delete($id = null) {

  //   if ($id === null) {
  //     $this->Flash->error('Invalid Arguments.');
  //     return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //   }

  //   $data = $this->Query->getAllDataById('Runners', [ 'Runners.id' => $id ]);
  //   if (isset($data['id'])) {
  //     $this->set('data', $data);
  //   } else {
  //     $this->Flash->error('Oops! Runner not found.');
  //     return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //   }

  //   $tmpData['id'] = $id;
  //   $tmpData['is_active'] = 0; //Soft Delete
  //   //Add User
  //   if ($this->Query->setData('Runners', $tmpData)) {
  //     $this->Flash->success('Runner `'.$data['name'].'` has been deleted.');
  //     return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //   } else {
  //     $this->Flash->error('Oops! Something went wrong. Please try again later.');
  //     return $this -> redirect(array('controller' => 'runners', 'action' => 'view'));
  //   }
    
  // }
}
?>