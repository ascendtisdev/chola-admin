<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Http\Exception\NotFoundException;
class FeedbacksController extends AppController
{
  public $components = ['Query','Special'];    
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * Add Feedback
   * 
   * @return json
   */
  public function add() {

    if ($this->request->is('post')) {

        $data = $this->request->getData();
        
        $data['user_id'] = $this->Auth->user('id');
        $data['is_active'] = 1;
        if ($this->Query->setData('Feedbacks', $data)) {

          $this->set([
            'success' => true,
            '_serialize' => ['success', 'data']
          ]);
  
        } else {
          throw new InternalErrorException('Oops! Something went wrong. Please try again later.');
        }
        

      }

    }
}
?>