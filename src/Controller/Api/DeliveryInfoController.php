<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class DeliveryInfoController extends AppController
{
    public $components = ['Query','Special','UserWallet'];    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([]);
    }

    /**
     * Get DeliveryInfo (Calendar)
     * 
     * @return json
     */
    public function getCalendar() {
      if ($this->request->is('post')) {
        $user = $this->Auth->user();

        $data = $this->Query->getAllData('UserSubscriptions',
          [
            'UserSubscriptions.user_id' => $user['id'],
            'UserSubscriptions.is_active' => 1,
            'UserSubscriptions.end_date >=' => date('Y-m-d')
          ],
          [],
          false,
          [
            'DeliveryInfo' => function($q) {
              $q->where(['DeliveryInfo.is_active' => 1]);
              $q->order(['DeliveryInfo.date asc']);
              return $q->autoFields(false);
            }
          ]
        );

        
        if (sizeof($data) > 0) {
          $delivered = $this->DeliveryInfo->find()->where(['DeliveryInfo.status' => 'DELIVERED','DeliveryInfo.user_id' => $user['id']]);
          $calendar = [];
          $deliveryInfo = $data[0]['delivery_info'];
          if (sizeof($deliveryInfo) > 0) {
            $calendar = [];
            foreach ($deliveryInfo as $key => $val) {
              $frm_date = date('Y-m-d',strtotime($val['date']));
              $calendar[$frm_date]['selected'] = true;

              if ($val['status'] === 'PENDING') {
                $calendar[$frm_date]['selectedColor'] = 'gray';
              } else if ($val['status'] === 'DELIVERED') {
                $calendar[$frm_date]['selectedColor'] = 'green';
              } else if ($val['status'] === 'NOT_DELIVERED') {
                $calendar[$frm_date]['selectedColor'] = 'red';
              } else if ($val['status'] === 'NOT_AVAILABLE') {
                $calendar[$frm_date]['selectedColor'] = 'orange';
              }
              $calendar[$frm_date]['data'] = $val;
            }
  
            $this->set([
              'success' => true,
              'data' => $data,
              'calendar' => $calendar,
              'deliveredCount' => $delivered->count(),
              '_serialize' => ['success','data','calendar','deliveredCount']
            ]);

          } else {
            throw new InternalErrorException('Oops! Something went wrong. Please try again later.');
          }
        } else {
          throw new NotFoundException('Oops! No Subscriptions found.');
        }
      }
    }


    /**
     * Get Runners Delivery
     * 
     * @return json
     */
    public function getDeliveryList() {
      if ($this->request->is('post')) {
        $data = $this->request->getData();
        $user = $this->Auth->user();

        $runner = $this->Query->getAllDataById('Runners',
          [
            'Runners.user_id' => $user['id']
          ],
          [
            'Runners.updated_at'
          ],
          false,
          [
            'Users'
          ]
        );
        
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');

        if (isset($data['start_date'])) {
          $start_date = $data['start_date'];
        }
        if (isset($data['end_date'])) {
          $end_date = $data['end_date'];
        }
        
        $deliveryList = $this->Query->getAllData('DeliveryInfo',
          [
            'DeliveryInfo.status' => 'PENDING',
            'DeliveryInfo.runner_id' => $runner['id'],
            'DeliveryInfo.date >=' => $start_date,
            'DeliveryInfo.date <=' => $end_date,
          ],
          [],
          false,
          ['Users']
        );

        if (sizeof($deliveryList) > 0) {
          $this->set([
            'success' => true,
            'data' => $deliveryList,
            '_serialize' => ['success','data']
          ]);
        } else {
          throw new NotFoundException('Oops! No delivery list found.');
        }
      }
    }

    /**
     * Marked as Delivered
     * 
     * @return json
     */
    public function markedAsDelivered() {
      if ($this->request->is('post')) {
        $data = $this->request->getData();

        if (!isset($data['id']) || empty($data['id'])) {
          throw new BadRequestException('Invalid arguments supplied.');
        }

        $deliveryInfo = $this->Query->getAllDataById('DeliveryInfo',
          [
            'DeliveryInfo.id' => $data['id']
          ],
          [],
          ['UserSubscriptions','Users']
        );

        $tmp = [];
        $tmp['id'] = $data['id'];
        $tmp['status'] = 'DELIVERED';

        if ($this->Query->setData('DeliveryInfo', $tmp)) {
          if (!$this->isDeliveryEntryExists($deliveryInfo['user_id'], $deliveryInfo['date'])) {
            //if false it means entry does not exists. - Then only insert.
            
            //Add Empty Bottles to User.
            $tmpData = [];
            $tmpData['id'] = $deliveryInfo['user_id'];
            $tmpData['empty_bottles'] = ($deliveryInfo['user']['empty_bottles'] + $deliveryInfo['user_subscription']['subscription_per_day_bottles_to_deliver']);
            $this->Query->setData('Users', $tmpData);
            //---=-=-=-=-=

            $this->UserWallet->giveUserPoints($deliveryInfo['user_id'], -$deliveryInfo['user_subscription']['subscription_per_day_price'], 'Milk Delivered on '.$deliveryInfo['date'], null, $deliveryInfo['date']);
            $this->set([
              'success' => true,
              '_serialize' => ['success']
            ]);
          } else {
            $this->set([
              'success' => true,
              '_serialize' => ['success']
            ]);
          }
        } else {
          throw new InternalErrorException('Oops! Something went wrong.');
        }
      }
    }

    /**
     *  Marked as UnDelivered
     * 
     * @return json
     */
    public function markedAsUnDelivered() {
      if ($this->request->is('post')) {
        $data = $this->request->getData();

        if (!isset($data['id']) || empty($data['id'])) {
          throw new BadRequestException('Invalid arguments supplied.');
        }

        $deliveryInfo = $this->Query->getAllDataById('DeliveryInfo',
          [
            'DeliveryInfo.id' => $data['id']
          ],
          [],
          ['UserSubscriptions']
        );

        $tmp = [];
        $tmp['id'] = $data['id'];
        $tmp['status'] = 'NOT_DELIVERED';

        if ($this->Query->setData('DeliveryInfo', $tmp)) {
          $this->set([
            'success' => true,
            '_serialize' => ['success']
          ]);
        } else {
          throw new InternalErrorException('Oops! Something went wrong.');
        }

      }
    }

  /**
   * Check in UserWallet, is entry existed for the day(delivery_date) of delivery or not
   * 
   * @return boolean
   */
  private function isDeliveryEntryExists($user_id = null, $date = null) {
    if ($date == null) {
      return false;
    }

    $userWallet = $this->Query->getDataById('UserWallets', 
      [
        'UserWallets.delivery_date' => $date,
        'UserWallets.user_id' => $user_id
      ]
    );

    if (isset($userWallet['id']) && $userWallet['id'] != null) {
      return true;
    } else {
      return false;
    }
  }

}