<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Http\Exception\NotFoundException;
class SubscriptionsController extends AppController
{
  public $components = ['Query','Special'];    
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * Get Subscriptions
   * 
   * @return json
   */
  public function get() {
    if ($this->request->is('post')) {
      
      $data = $this->Query->getAllData('Subscriptions',
        [
          'Subscriptions.is_active' => 1
        ]
      );

      $finalData = [];
      if (sizeof($data) > 0) {
        foreach ($data as $key => $val) {
          $tmp = [];
          $tmp['id'] = $val['id'];
          $tmp['name'] = $val['name'];
          $tmp['description'] = $val['description'];
          $tmp['qty'] = $val['qty'];
          $tmp['price'] = $val['price'];
          $tmp['image_url'] = $this->Special->getBaseImageUrl().$val['photo_dir'].$val['photo'];
          $tmp['sm_image_url'] = $this->Special->getBaseImageUrl().$val['photo_dir'].'sm_'.$val['photo'];

          array_push($finalData, $tmp);
        }

        $this->set([
          'success' => true,
          'data' => $finalData,
          '_serialize' => ['success', 'data']
        ]);

      } else {
        throw new NotFoundException('Oops! No Subscriptions found.');
      }

    }
  }
}
?>