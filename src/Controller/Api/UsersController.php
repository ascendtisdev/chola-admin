<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\InternalErrorException;
use Cake\Network\Exception\ForbiddenException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    public $components = ['Query','Special','SMS'];    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'login','verify']);
    }

    // public function add()
    // {
    //     $this->Crud->on('afterSave', function(Event $event) {
    //         if ($event->subject->created) {
    //             $this->set('data', [
    //                 'id' => $event->subject->entity->id,
    //                 'token' => JWT::encode(
    //                     [
    //                         'sub' => $event->subject->entity->id,
    //                         'exp' =>  time() + 604800
    //                     ],
    //                 Security::salt())
    //             ]);
    //             $this->Crud->action()->config('serialize.data', 'data');
    //         }
    //     });
    //     return $this->Crud->execute();
    // }
    
    /**
     * Get User Profile
     * 
     * @return json
     */
    public function get() {
        if ($this->request->is('post')) {

            $user = $this->Query->getAllDataById('Users',
                [
                    'Users.id' => $this->Auth->user('id')
                ],
                [],
                ['Runners']
            );

            $this->set([
				'success' => true,
				'data' => $user,
				'_serialize' => ['success','data']
			]);
        }
    }


    /**
     * Generate Token
     */
    public function token()
    {
        // $user = $this->Auth->identify();
        if ($this->request->is('post')) {
          $data = $this->request->getData();
        
          if (!isset($data['username']) || empty($data['username'])) {
						throw new BadRequestException('Invalid arguments supplied.');
					}

          $user = $this->Query->getDataById('Users',
            [
              'Users.username' => $data['username']
						],
						[
							'id',
							'username'
						]
          );
          
          $this->set([
              'success' => true,
              'data' => [
                  'token' => JWT::encode([
                        'sub' => $user['id'],
                      'exp' =>  time() + 604800
                  ],
                  Security::salt())
              ],
              '_serialize' => ['success', 'data']
          ]);  
        }
		}
		
        /**
         * Login User (Send OTP)
         * 
         * @return json
         */
		public function login() {
			if ($this->request->is('post')) {
				$data = $this->request->getData();

				if (!isset($data['username']) || empty($data['username'])) {
					throw new BadRequestException('Invalid arguments supplied.');
				}

				$user = $this->Query->getDataById('Users',
					[
						'Users.username' => $data['username']
					],
					[
						'id',
                        'username',
                        'name',
                        'Runners.id'
                    ],
                    ['Users.created_at'],
                    ['Runners']
                );
                
                /**
                 * Normal Users
                 */
                //Uncomment this Line Later
                //$otp = $this->Special->generateNumber(4);
                $otp = '1234';
				if (isset($user['id']) && !empty($user['id'])) {
					//UserExists
					//SendOTP To His Mobile Number
                    //Update LastOTP in database along with it's Expiry date
                    $tmpData = [];
                    $tmpData['id'] = $user['id'];
                    $tmpData['otp'] = $otp;
                    $tmpData['otp_valid_upto'] = date("Y-m-d H:i:s", strtotime("+30 minutes"));
                    
                    if ($this->Query->setData('Users', $tmpData)) {
                        $tmpData['username'] = $data['username'];
                        $tmpData['name'] = $user['name'];
                        
                        //Uncomment this Line Later
                        // $this->SMS->sendUserOTP($tmpData);

                        $this->set([
                            'success' => true,
                            'data' => $user,
                            'mode' => 'LOGIN',
                            '_serialize' => ['success','data','mode']
                        ]);            
                    } else {
                        throw new InternalErrorException('Oops! Something went wrong.');
                    }
				} else {
					//UserNotExists
                    //CreateUser
                    //Update LastOTP in database along with it's Expiry date
                    $tmpData = [];
                    $tmpData['username'] = $data['username'];
                    $tmpData['name'] = '';  //to Ignore Name in SMS
                    $tmpData['otp'] = $otp;
                    $tmpData['otp_valid_upto'] = date("Y-m-d H:i:s", strtotime("+30 minutes"));
                    $tmpData['is_active'] = 1;

                    if ($this->Query->setData('Users', $tmpData)) {                        
                        //Uncomment this Line Later
                        // $this->SMS->sendUserOTP($tmpData);

                        $this->set([
                            'success' => true,
                            'mode' => 'SIGNUP',
                            '_serialize' => ['success','mode']
                        ]);            
                    } else {
                        throw new InternalErrorException('Oops! Something went wrong.');
                    }
				}
			}
        }
        
        /**
         * Verify User (OTP)
         * 
         * @return json
         */
        public function verify() {
					if ($this->request->is('post')) {
							$data = $this->request->getData();
							if (!isset($data['username']) || empty($data['username']) 
									|| !isset($data['otp']) || empty($data['otp'])) {
								throw new BadRequestException('Invalid arguments supplied.');
							}
							
							$user = $this->Query->getAllDataById('Users',
								[
									'Users.username' => $data['username'],
									'Users.otp' => $data['otp'],
									'Users.otp_valid_upto >' => date("Y-m-d H:i:s")
								],
								[],
								['Runners']
							);

							if (isset($user['id']) && !empty($user['id'])) {
									//Also Send No. of Active UserSubscriptions so that we can redirect user to Home if there is more than 0 or else to Subscriptions Page.

									$userSubscription = $this->Query->getData('UserSubscriptions',
											[
													'UserSubscriptions.user_id' => $user['id'],
													'UserSubscriptions.is_active' => 1,
													'UserSubscriptions.end_date >' => date("Y-m-d H:i:s")
											],
											[
													'UserSubscriptions.subscription_name'
											]
									);

									$subscription = [];
									$subscriptionCount = sizeof($userSubscription);
									if ($subscriptionCount > 0) {
											$subscription = $userSubscription[0];
									}

									if (!empty($user['name'])) {
											$this->set([
													'success' => true,
													'data' => [
															'user'  => $user,
															'subscription' => $subscription,
															'subscriptions_count' => $subscriptionCount,
															'token' => JWT::encode([
																	'sub' => $user['id'],
																	'exp' =>  time() + (86400 * 30) //1 day = 86400
															],
															Security::salt())
													],
													'mode' => 'LOGIN',
													'_serialize' => ['success', 'data','mode']
											]);
									} else {
											$this->set([
													'success' => true,
													'data' => [
															'user'  => $user,
															'token' => JWT::encode([
																	'sub' => $user['id'],
																	'exp' =>  time() + (86400 * 30) //1 day = 86400
															],
															Security::salt())
													],
													'mode' => 'SIGNUP',
													'_serialize' => ['success', 'data','mode']
											]);
									}
							} else {
									//If 3 Times OTP is Invalid Then Redirect User to Login Page.
									throw new ForbiddenException('Oops! Invalid OTP.');
							}
					}
			}
				
		public function updateEmptyBottles() {
			if ($this->request->is('post')) {
				$data = $this->request->getData();

				if (!isset($data['user_id']) || !isset($data['empty_bottles'])) {
					throw new BadRequestException('Invalid arguments supplied.');
				}
				
				$user = $this->Query->getAllDataById('Users', ['Users.id' => $data['user_id']]);
				$tmp = [];
				$tmp['id'] = $data['user_id'];
				$tmp['empty_bottles'] = ($user['empty_bottles'] - $data['empty_bottles']);
				
				if ($this->Query->setData('Users', $tmp)) {
					$this->set([
						'success' => true,
						'data' => $tmp['empty_bottles'],
						'_serialize' => ['success','data']
					]);		
				} else {
					throw new InternalErrorException('Oops! Something went wrong.');
				}

			}
		}

		public function test() {

			$this->set([
				'success' => true,
				'data' => $this->Auth->user(),
				'_serialize' => ['success','data']
			]);
		}


}