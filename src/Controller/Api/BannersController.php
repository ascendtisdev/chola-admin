<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Http\Exception\NotFoundException;
class BannersController extends AppController
{
  public $components = ['Query','Special'];    
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * Get Banners
   * 
   * @return json
   */
  public function get() {
    if ($this->request->is('post')) {
      
      $data = $this->Query->getAllData('Banners',
        [
          'Banners.is_active' => 1
        ],
        [
          'Banners.precedence'
        ]
      );

      $finalData = [];
      if (sizeof($data) > 0) {
        foreach ($data as $key => $val) {
          $tmp = [];
          $tmp['id'] = $val['id'];
          $tmp['title'] = $val['title'];
          $tmp['precedence'] = $val['precedence'];
          $tmp['image_url'] = $this->Special->getBaseImageUrl().$val['photo_dir'].$val['photo'];
          $tmp['sm_image_url'] = $this->Special->getBaseImageUrl().$val['photo_dir'].'sm_'.$val['photo'];
          $tmp['md_image_url'] = $this->Special->getBaseImageUrl().$val['photo_dir'].'md_'.$val['photo'];
          array_push($finalData, $tmp);
        }

        $this->set([
          'success' => true,
          'data' => $finalData,
          '_serialize' => ['success', 'data']
        ]);

      } else {
        throw new NotFoundException('Oops! No Banners found.');
      }

    }
  }
}
?>