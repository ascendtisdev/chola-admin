<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Feedbacks
 * @package   Feedbacks
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2018 Copyright (c) Ascendtis.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;

use App\Controller\AppController;

/**
 * Feedbacks Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Feedbacks
 * @package  Feedbacks
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */
class FeedbacksController extends AppController
{
  public $components = ['Query','Paginator'];
  
  /**
   *   Initialization
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow([]);
  }

  /**
   * View Feedbacks
   * 
   * @return void
   */
  public function index() {
    $this->viewBuilder()->setLayout('base_layout');

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 20,
      'conditions' => [
        'Feedbacks.is_active' => 1
      ],
      'contain' => [
        'Users'
      ]
    ];
  
    $details=$this->Feedbacks->find('all');
    $this->set('data', $this->paginate($details));
  }

   /**
   * Delete Feedback
   * 
   * @return void
   */
  public function delete($id = null) {

    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this -> redirect(array('controller' => 'feedbacks', 'action' => 'index'));
    }

    $data = $this->Query->getAllDataById('Feedbacks', [ 'Feedbacks.id' => $id ]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Runner not found.');
      return $this -> redirect(array('controller' => 'feedbacks', 'action' => 'index'));
    }

    $tmpData['id'] = $id;
    $tmpData['is_active'] = 0; //Soft Delete
    //Add User
    if ($this->Query->setData('Feedbacks', $tmpData)) {
      $this->Flash->success('Feedback has been deleted.');
      return $this -> redirect(array('controller' => 'feedbacks', 'action' => 'index'));
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
      return $this -> redirect(array('controller' => 'feedbacks', 'action' => 'index'));
    }
    
  }

}
?>