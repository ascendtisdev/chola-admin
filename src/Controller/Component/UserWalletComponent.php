<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   UserWallet
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * Constants Variables
 *
 * @category Constants
 * @package  UserWallet
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */
interface Enum
{
    //---- constants in place of enumerations
    const OPERATION_FAILED = "Operation failed. Please Try again.";
    const USER_NOT_EXISTS = "Oops! User does not exists.";
    const SUCCESS = true;
    const FAILED = false;
    const ZERO_POINTS = 0.00;
    const INVALID_ARGUMENT = "INVALID ARGUMENT.";
}


/**
 * UserWallet Component
 *
 * @category Component
 * @package  UserWallet
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class UserWalletComponent extends Component implements Enum
{
    public $components = ['Special','Query'];

    /**
     *   Give User Points ( LS Money )
     * 
     * @UpdatedOn      25th Oct 2018
     * @param uuid     $user_id        User ID
     * @param decimal  $points         Points/Wallet Money
     * @param string   $reason         Reason for this transaction.
     * @param uuid     $transaction_id Transaction ID
     * @param datetime $expires        Expires particular entry At.
     *
     * @return boolean
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function giveUserPoints($user_id = null, $points = 0.00, $reason = null,
        $transaction_id = null, $delivery_date = null, $expires = null
    ) {

        if ($expires == null) {
            //validation or defaults presets
            $expires = date('Y-m-d h:m:s', strtotime('+10 years'));
        }

        $transaction_type = ($points < 0 ? 'DEBIT' : 'CREDIT'); //0 - debit, 1 - credit

        $isUser = $this->Query->getAllDataById('Users',
            [
                'Users.id' => $user_id
            ]
        );
        if (isset($isUser['id']) && $isUser['id'] !== null) {
            $userWallet = [];
            $userWallet['amount'] = $points;
            $userWallet['user_id'] = $user_id;
            $userWallet['transaction_type'] = $transaction_type;
            $userWallet['expires'] = $expires;
            $userWallet['transaction_id'] = $transaction_id;
            $userWallet['reason'] = $reason;
            $userWallet['delivery_date'] = $delivery_date;

            if ($this->Query->setData('UserWallets',$userWallet)) {
                //After Inserting Calculate Sum of Amount in his Wallet and Update UsersTable 'wallet_balance'
                $amount = $this->_calcPoints($user_id);
                $userData = [];
                $userData['id'] = $user_id;
                $userData['wallet_balance'] = $amount;

                if ($this->Query->setData('Users', $userData)) {
                    return Enum::SUCCESS;
                } else {
                    return Enum::FAILED;
                }

            } else {
                return Enum::FAILED;
            }

        } else {
            return Enum::FAILED;
        }
    }

    /**
     *   Get User Points ( Wallet Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getUserPoints($user_id = null)
    {
        $isUser = $this->Query->getDataById('Users',
            [
                'Users.id' => $user_id
            ]
        );

        if (isset($isUser['id']) && $isUser['id'] !== null) {
            return $this->_calcPoints($user_id);
        } else {
            return Enum::USER_NOT_EXISTS;
        }
    }


    /**
     *   Get All Points ( LS Money )
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function getAllPoints()
    {
        return $this->_calcAllPoints();
    }


    /**
     *   Get All User Wallet Logs
     *
     * @param uuid    $user_id User ID
     * @param integer $format  Type (Json/Array)
     *
     * @return array
     */
    // public function getUserWalletLogs($user_id = null, $format = 0)
    // {
    //     try {
    //         //Model->TableName
    //         $user_wallets = TableRegistry::get('UserWallets');

    //         $logs = $user_wallets->find()
    //             ->where(
    //                 [
    //                     'user_id' => $user_id
    //                 ]
    //             )
    //             ->toArray();

    //         if ($format === 0) {
    //             return $logs;
    //         } else if ($format == 1) {
    //             return json_encode($logs);
    //         } else {
    //             return Enum::INVALID_ARGUMENT;
    //         }
    //     } catch(Exception $err) {
    //         return Enum::FAILED;
    //     }
    // }


    /**
     *   Calculate Overall user Points ( Wallet Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _calcPoints($user_id = null)
    {
        try {
            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');
            $curr_date = date('Y-m-d h:m:s');

            $query = $user_wallets->find();
            $points = $query->select(
                [
                    'sum'=> $query->func()->sum('amount')
                ]
            )
                ->where(
                    [
                    'user_id' => $user_id,
                    'expires >=' => $curr_date
                    ]
                )
                ->toArray();
            if (!isset($points[0]['sum'])) {
                return Enum::ZERO_POINTS;
            } else {
                return $points[0]['sum'];
            }

        } catch(Exception $err) {
            return Enum::FAILED;
        }
    }


    /**
     *   Calculate Overall Points ( LS Money )
     *
     * @param uuid $user_id User ID
     *
     * @return decimal
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function _calcAllPoints($user_id = null)
    {
        try {
            //Model->TableName
            $user_wallets = TableRegistry::get('UserWallets');
            $curr_date = date('Y-m-d h:m:s');

            $query = $user_wallets->find();
            $points = $query->select(
                [
                    'sum'=> $query->func()->sum('amount')
                ]
            )
                ->where(
                    [
                    'expires >=' => $curr_date
                    ]
                )
                ->toArray();
            if (!isset($points[0]['sum'])) {
                return Enum::ZERO_POINTS;
            } else {
                return $points[0]['sum'];
            }

        } catch(Exception $err) {
            return Enum::FAILED;
        }
    }

}
?>