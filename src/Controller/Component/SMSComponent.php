<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   SMS
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2017 Copyright (c) Actonate Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Network\Http\Client;
use Cake\Core\Configure;

/**
 * SMS Component
 *
 * @category Component
 * @package  SMS
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */

class SMSComponent extends Component
{
    public $components = ['Special'];

    /**
     *  Initialization
     *    DATE: 24th April 2017
     *
     * @return array
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    public function __construct()
    {
        $this->password = env('MSG91_SMS_PASSWORD');
        $this->senderID = env('MSG91_SMS_SENDERID');
        $this->route = env('MSG91_SMS_ROUTE');
        $this->url = env('MSG91_SMS_URL');

        //msg91
        $this->baseUrl = $this->url."?";
        $this->baseUrl .= "&authkey=".$this->password;
        $this->baseUrl .= "&sender=".$this->senderID;
        $this->baseUrl .= "&route=".$this->route;
        $this->baseUrl .= "&country=91";
        $this->baseUrl .= "&mobiles=";
    }

    /**
     *   Send SMS
     *
     * @param string $url URL
     *
     * @return json
     */
    private function _send($url = null)
    {
        if ($url != null) {
            $http = new Client();
            $response = $http->get($url);

            return $response->json;
        }
    }

    /**
     *   Send User OTP 
     *
     * @param array $data
     *
     * @return json
     */
    public function sendUserOTP($data = [])
    {
        $template = $this->baseUrl;
        $template .= $data['username'];
        $template .= "&message=Hello ".$data['name'].",";
        $template .= " Your mobile number verification code is ";
        $template .= $data['otp'];
        $template .= " for confirming Chola A2 Milk. This OTP code is valid for 30 minutes.";
        $template .= " Thanks!";

        $response = $this->_send($template);
        return $response;
    }

}
?>