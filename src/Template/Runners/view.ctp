 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Runners</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Runners</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <div class="container-fluid">

      <!-- Default box -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">View Runners</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

        <!-- DataTable -->

        <div class="dataTables_wrapper dt-bootstrap4">

          <div class="row">
            <div class="col-sm-12">
              <table id="main-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Address</th>
                  <th>Registered At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>

              <?php foreach ($data as $key=>$val): ?>
                <tr>
                  <td><?= $val['user']['name'] ?></td>
                  <td><?= $val['user']['username'] ?></td>
                  <td><?= $val['user']['address'] ?></td>
                  <td><?= date('jS F Y',strtotime( $val['created_at']) ) ?></td>
                  <td class="action-btn-container">
                    <a href="<?= $this->Url->build(["controller" => "users", "action" => "edit", $val['user']['id']]);?>" class="btn btn-sm btn-info action-btn" data-toggle="tooltip" title="Edit Runner"><i class="fas fa-pencil-alt"></i></a>
                    <a href="<?= $this->Url->build(["controller" => "runners", "action" => "delete", $val['id']]);?>" class="btn btn-sm btn-danger action-btn" onclick="return confirm('Are you sure? This action wont be rollback.')" data-toggle="tooltip" title="Delete Runner"><i class="fas fa-times"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
               
              </tbody>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Address</th>
                  <th>Registered At</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
   
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info">
                <?= $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries') ?>
              </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers d-flex justify-content-end">
                <ul class="pagination">
                  <?php
                    echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                  ?>
                </ul>
              </div>
            </div>
          </div>

        </div>
        <!-- DataTable Ends Here -->

        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    <!-- FAB -->
    <!-- <div class="floating-btn">
      <a href="<?= $this->Url->build(["controller" => "runners", "action" => "add"]);?>" class="btn btn-sm btn-primary floating-action-btn" data-toggle="tooltip" title="Add Runner">
        <i class="fa fa-plus"></i>
      </a>
    </div> -->
    <!-- FAB ENDS -->
  </div>
</section>