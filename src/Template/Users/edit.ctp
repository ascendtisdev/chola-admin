 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit User</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Users</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-6">
      <?= $this->Form->create('User', ['role'=>'form']); ?>
        <div class="card card-primary">
        <!-- Header -->
          <div class="card-header">
            <h3 class="card-title">Edit User</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>

          <!-- Body -->
          <div class="card-body" style="display: block;">
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?= $data['name'] ?>" required>
            </div>

            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" value="<?= $data['username'] ?>" required>
            </div>

            <div class="form-group">
              <label for="address">Address</label>
              <textarea class="form-control" rows="3" id="address" placeholder="Enter Address" name="address" required><?= $data['address'] ?></textarea>
            </div>

            <div class="form-group">
              <label for="pincode">Zipcode</label>
              <input type="text" class="form-control" id="pincode" placeholder="Enter Zipcode (Pincode)" value="<?= $data['pincode'] ?>" name="pincode" required>
            </div>

            <div class="form-group">
              <label for="empty_bottles">Empty Bottles</label>
              <input type="text" class="form-control" id="empty_bottles" placeholder="Enter Empty Bottles" value="<?= $data['empty_bottles'] ?>" name="empty_bottles" required>
            </div>

          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <button type="submit" class="btn btn-primary">Edit User</button>
          </div>
          <!-- /.card-footer-->
        </div> <!-- Card Ends -->
        </form>
      </div>
    </div>
  </div>
</section>