 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Users</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Users</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <div class="container-fluid">

      <!-- Default box -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">View Users</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

        <!-- DataTable -->

        <div class="dataTables_wrapper dt-bootstrap4">

          <div class="row">
            <div class="col-sm-12">
              <table id="main-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Wallet Balance (₹)</th>
                  <th>Empty <br/>Bottles</th>
                  <th>Address</th>
                  <th>Registered At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>

              <?php foreach ($data as $key=>$val): ?>
                <tr>
                  <td>
                    <?= $val['name'] ?>
                    <?php if (!empty($val['user_subscription'])): ?>
                      <br/>
                      <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#viewSubscription<?= $val['id'] ?>"><?= $val['user_subscription']['subscription_name'] ?></button>
                      <?= $this->element('UserSubscription/ViewSubscriptionModal',['user' => $val]) ?>
                    <?php else: ?>
                      <br/>
                      <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#addSubscription<?= $val['id'] ?>">Add Subscription</button>
                      <?= $this->element('UserSubscription/AddSubscriptionModal',['user' => $val,'subscriptions' => $subscriptions]) ?>
                    <?php endif;  ?>
                    <br/>
                    <?php if ($val['user_subscription']['payment_status'] === 'PAID'): ?>
                      <span class="badge badge-success"><?= $val['user_subscription']['payment_status'] ?></span>
                    <?php else: ?>
                      <span class="badge badge-danger"><?= $val['user_subscription']['payment_status'] ?></span>
                    <?php endif; ?>
                  </td>
                  <td><?= $val['username'] ?></td>
                  <td><span class="badge badge-success"><?= $val['wallet_balance'] ?></span></td>
                  <td><?= $val['empty_bottles'] ?></td>
                  <td><?= $val['address'] ?></td>
                  <td><?= date('jS F Y',strtotime( $val['created_at']) ) ?></td>
                  <td class="action-btn-container">
                    <a href="<?= $this->Url->build(["controller" => "users", "action" => "edit", $val['id']]);?>" class="btn btn-sm btn-info action-btn" data-toggle="tooltip" title="Edit User"><i class="fas fa-pencil-alt"></i></a>
                    <a href="<?= $this->Url->build(["controller" => "users", "action" => "delete", $val['id']]);?>" class="btn btn-sm btn-danger action-btn" onclick="return confirm('Are you sure? This action wont be rollback.')" data-toggle="tooltip" title="Delete User"><i class="fas fa-times"></i></a>
                    <?php if (empty($val['runner'])): ?>
                      <a href="<?= $this->Url->build(["controller" => "runners", "action" => "add", $val['id']]);?>" class="btn btn-sm btn-primary action-btn" onclick="return confirm('Are you sure? You want to add this user as DeliveryBoy.')" data-toggle="tooltip" title="Add as Runner"><i class="fas fa-plus"></i></a>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
               
              </tbody>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Wallet Balance (₹)</th>
                  <th>Empty <br/>Bottles</th>
                  <th>Address</th>
                  <th>Registered At</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
   
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info">
                <?= $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries') ?>
              </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers d-flex justify-content-end">
                <ul class="pagination">
                  <?php
                    echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                  ?>
                </ul>
              </div>
            </div>
          </div>

        </div>
        <!-- DataTable Ends Here -->

        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    <!-- FAB -->
    <div class="floating-btn">
      <a href="<?= $this->Url->build(["controller" => "users", "action" => "add"]);?>" class="btn btn-sm btn-primary floating-action-btn" data-toggle="tooltip" title="Add User">
        <i class="fa fa-plus"></i>
      </a>
    </div>
    <!-- FAB ENDS -->
  </div>
</section>