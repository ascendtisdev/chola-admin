 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Add Banner</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Banners</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-6">
      <?= $this->Form->create('Banner', ['role'=>'form','type' => 'file']); ?>
        <div class="card card-primary">
        <!-- Header -->
          <div class="card-header">
            <h3 class="card-title">Add Banner</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>

          <!-- Body -->
          <div class="card-body" style="display: block;">
            <div class="form-group">
              <label for="name">Title</label>
              <input type="text" class="form-control" id="name" placeholder="Enter Title" name="title" required>
            </div>

            <div class="form-group">
              <label for="qty">Percedence (Priority)</label>
              <input type="text" class="form-control" id="qty" placeholder="Enter Priority" name="precedence" required>
            </div>

            <div class="form-group">
              <label for="description">Primary Photo:</label>
              <?php echo $this->Form->control('photo', ['type' => 'file','label'=>false]); ?>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <button type="submit" class="btn btn-primary">Add Banner</button>
          </div>
          <!-- /.card-footer-->
        </div> <!-- Card Ends -->
        </form>
      </div>
    </div>
  </div>
</section>