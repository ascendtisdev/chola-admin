<!-- Modal -->
<div class="modal fade" id="editDeliveryInfo<?=$data['id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="editDeliveryInfoTitle" aria-hidden="true">
  <?= $this->Form->create('DeliveryInfo', ['role'=>'form','url'=>['controller'=>'delivery_info','action'=>'edit']]); ?>“
  <div class="modal-dialog modal-dialog-centered normal-modal-size" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editDeliveryInfoTitle">Edit DeliveryInfo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col">

            <div class="card-body" style="display: block;">

              <div class="form-group">
                <label for="username">Date</label>
                <input id="start_date" class="material-datepicker form-control" name="start_date" value="<?= date('dS F Y', strtotime($data['date'])) ?>" disabled/>
              </div>

              <div class="form-group">
                <label for="status">Delivery Status</label>
                <?= $this->Form->select('status',
                    [
                      'PENDING' => 'Pending',
                      'DELIVERED' =>  'Delivered',
                      'NOT_DELIVERED' => 'Not Delivered',
                      'NOT_AVAILABLE' => 'Not Available'
                    ],
                    [
                      'id' => 'status',
                      'empty' => '(Choose delivery status)',
                      'class' => 'form-control',
                      'required' => 'required',
                      'value' => $data['status']
                    ]
                )?>
              </div>

              <div class="form-group">
                <label for="delivery_slot">Delivery Slot</label>
                <?= $this->Form->select('delivery_slot',
                    [
                      'MORNING' => 'Morning (7 AM - 9 AM)',
                      'EVENING' =>  'Evening (6 PM - 8 PM)'
                    ],
                    [
                      'id' => 'delivery_slot',
                      'empty' => '(Choose delivery slot)',
                      'class' => 'form-control',
                      'required' => 'required',
                      'value' => $data['delivery_slot']
                    ]
                )?>
              </div>

              <!-- Hidden Fields -->
              <input type="hidden" class="form-control" id="id" name="id" value="<?= $data['id'] ?>">

            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Edit Delivery Info</button>
      </div>

    </div>
  </div>
  </form>
</div>
