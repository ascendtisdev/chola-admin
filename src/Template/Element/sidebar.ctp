 <?php
  $action = $this->request->getParam('action');
  $controller = $this->request->getParam('controller');
 ?>
 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link bg-primary">
      <!-- <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <span class="brand-text font-weight-light">Asgard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

        <li class="nav-header">HOME</li>

        <li class="nav-item">
          <a href="<?= $this->Url->build(["controller" => "admins", "action" => "dashboard"]);?>" class="nav-link <?= $controller==='Admins' && $action === 'dashboard' ? 'active': null; ?>">
            <i class="nav-icon fa fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview <?= $controller==='Admins' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Admins' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-user-secret"></i>
            <p>
              Admins
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "admins", "action" => "add"]);?>" class="nav-link <?= $controller==='Admins' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "admins", "action" => "view"]);?>" class="nav-link <?= $controller==='Admins' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li>


        <li class="nav-item has-treeview <?= $controller==='Users' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Users' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-user"></i>
            <p>
              Users
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "users", "action" => "add"]);?>" class="nav-link <?= $controller==='Users' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "users", "action" => "view"]);?>" class="nav-link <?= $controller==='Users' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li>

        <li class="nav-item">
          <a href="<?= $this->Url->build(["controller" => "delivery_info", "action" => "index"]);?>" class="nav-link <?= $controller==='DeliveryInfo' && $action === 'index' ? 'active': null; ?>">
            <i class="nav-icon fas fa-clock"></i>
            <p>
              Delivery Schedule
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview <?= $controller==='Subscriptions' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Subscriptions' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-file-contract"></i>
            <p>
              Subscriptions
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "subscriptions", "action" => "add"]);?>" class="nav-link <?= $controller==='Subscriptions' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "subscriptions", "action" => "view"]);?>" class="nav-link <?= $controller==='Subscriptions' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li>


        <li class="nav-item has-treeview <?= $controller==='Products' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Products' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-cart-plus"></i>
            <p>
              Products
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "products", "action" => "add"]);?>" class="nav-link <?= $controller==='Products' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "products", "action" => "view"]);?>" class="nav-link <?= $controller==='Products' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li>

        <!-- <li class="nav-item has-treeview <?= $controller==='Runners' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Runners' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-truck"></i>
            <p>
              Runners
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "runners", "action" => "add"]);?>" class="nav-link <?= $controller==='Runners' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "runners", "action" => "view"]);?>" class="nav-link <?= $controller==='Runners' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li> -->

        <li class="nav-item">
          <a href="<?= $this->Url->build(["controller" => "runners", "action" => "view"]);?>" class="nav-link <?= $controller==='Runners' && $action === 'view' ? 'active': null; ?>">
            <i class="nav-icon fas fa-truck"></i>
            <p>
              Runners
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview <?= $controller==='Banners' && ($action === 'add' || $action === 'view')  ? 'menu-open': null; ?>">
          <a href="#" class="nav-link <?= $controller==='Banners' && ($action === 'add' || $action === 'view')  ? 'active': null; ?>">
            <i class="nav-icon fas fa-images"></i>
            <p>
              Banners
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "banners", "action" => "add"]);?>" class="nav-link <?= $controller==='Banners' && $action === 'add' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= $this->Url->build(["controller" => "banners", "action" => "view"]);?>" class="nav-link <?= $controller==='Banners' && $action === 'view' ? 'active': null; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View</p>
                </a>
              </li>
            </ul>
        </li>

        <li class="nav-item">
          <a href="<?= $this->Url->build(["controller" => "feedbacks", "action" => "index"]);?>" class="nav-link <?= $controller==='Feedbacks' && $action === 'index' ? 'active': null; ?>">
            <i class="nav-icon fas fa-comments"></i>
            <p>
              Feedbacks
            </p>
          </a>
        </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
