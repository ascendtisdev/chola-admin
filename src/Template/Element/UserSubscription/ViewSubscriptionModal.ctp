<!-- Modal -->
<div class="modal fade" id="viewSubscription<?=$user['id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="viewSubscriptionTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered normal-modal-size" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="viewSubscriptionTitle">View Subscription (<?= $user['name'] ?>)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row no-gutters">
        <div class="col-6">
          <div class="card"> <!-- style="width: 18rem;" -->
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><b>Subscription</b></li>
              <li class="list-group-item"><b>Price</b></li>
              <li class="list-group-item"><b>Qty.</b></li>
              <li class="list-group-item"><b>Start - End Date</b></li>
              <li class="list-group-item"><b>Validity</b></li>
              <li class="list-group-item"><b>Remaining days</b></li>
              <li class="list-group-item"><b>Delivery Slot</b></li>
              <li class="list-group-item"><b>Payment Status</b></li>

            </ul>
          </div>
        </div>

        <div class="col-6">
          <div class="card"><!-- style="width: 18rem;" -->
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><?= $user['user_subscription']['subscription_name'] ?></li>
              <li class="list-group-item">Rs. <?= $user['user_subscription']['subscription_price'] ?></li>
              <li class="list-group-item"><?= $user['user_subscription']['qty'] ?>ml</li>
              <li class="list-group-item"><?= date('d/m/Y',strtotime($user['user_subscription']['start_date'])) ?> <b>-</b> <?= date('d/m/Y', strtotime($user['user_subscription']['end_date'])) ?></li>
              <li class="list-group-item"><?= $user['user_subscription']['days'] ?> days</li>
              <li class="list-group-item">
                <?php
                  $today = new DateTime();
                  $end_date = new DateTime($user['user_subscription']['end_date']);
                  $interval = $today->diff($end_date);
                  echo $interval->days;
                ?>
              </li>
              <li class="list-group-item">
                <?= $user['user_subscription']['delivery_slot'] ?>
              </li>
              <li class="list-group-item">
                <?= $user['user_subscription']['payment_status'] ?>
              </li>
            </ul>
          </div>
        </div>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>