<!-- Modal -->
<div class="modal fade" id="addSubscription<?=$user['id']?>" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="addSubscriptionTitle" aria-hidden="true">
  <?= $this->Form->create('UserSubscription', ['role'=>'form','url'=>['controller'=>'user_subscriptions','action'=>'add']]); ?>“
  <div class="modal-dialog modal-dialog-centered normal-modal-size" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addSubscriptionTitle">Add Subscription (<?= $user['name'] ?>)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col">

            <div class="card-body" style="display: block;">
              <div class="form-group">
                <label for="name">Subscription</label>
                <?= $this->Form->select('subscription_id',$subscriptions,
                    [
                      'empty' => '(Choose subscription)',
                      'class' => 'form-control',
                      'required' => 'required'
                    ]
                )?>

              </div>

              <div class="form-group">
                <label for="username">Start Date</label>
                <input id="start_date" class="material-datepicker form-control" name="start_date" value="<?= date('Y-m-d') ?>"/>
              </div>

              <div class="form-group">
                <label for="address">End Date</label>
                <input id="end_date" class="material-datepicker form-control" name="end_date" value="<?= date('Y-m-d',strtotime(date('Y-m-d').' +30 days')) ?>"/>
              </div>


              <div class="form-group">
                <label for="payment_status">Payment Status</label>
                <?= $this->Form->select('payment_status',
                    [
                      'PENDING' => 'Pending',
                      'PAID' =>  'Paid'
                    ],
                    [
                      'id' => 'payment_status',
                      'empty' => '(Choose payment status)',
                      'class' => 'form-control',
                      'required' => 'required'
                    ]
                )?>

              </div>

              <div class="form-group">
                <label for="delivery_slot">Delivery Slot</label>
                <?= $this->Form->select('delivery_slot',
                    [
                      'MORNING' => 'Morning (7 AM - 9 AM)',
                      'EVENING' =>  'Evening (6 PM - 8 PM)'
                    ],
                    [
                      'id' => 'delivery_slot',
                      'empty' => '(Choose delivery slot)',
                      'class' => 'form-control',
                      'required' => 'required'
                    ]
                )?>
              </div>


              <!-- Hidden Fields -->
              <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?= $user['id'] ?>">

            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add Subscription</button>
      </div>

    </div>
  </div>
  </form>
</div>
