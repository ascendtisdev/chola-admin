 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Subscription</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Subscriptions</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-6">
      <?= $this->Form->create('Subscription', ['role'=>'form','type' => 'file']); ?>
        <div class="card card-primary">
        <!-- Header -->
          <div class="card-header">
            <h3 class="card-title">Edit Subscription</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>

          <!-- Body -->
          <div class="card-body" style="display: block;">
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?= $data['name'] ?>" required>
            </div>

            <div class="form-group">
              <label for="qty">Qty (ML)</label>
              <input type="text" class="form-control" id="qty" placeholder="Enter Qty" name="qty" value="<?= $data['qty'] ?>" required>
            </div>

            <div class="form-group">
              <label for="price">Price</label>
              <input type="number" class="form-control" id="price" placeholder="Enter Price" name="price" value="<?= $data['price'] ?>" required>
            </div>

            <div class="form-group">
              <label for="per_day_price">Per Day (Price)</label>
              <input type="number" class="form-control" id="per_day_price" placeholder="Enter Per Day Price" name="per_day_price" value="<?= $data['per_day_price'] ?>" required>
            </div>

            <div class="form-group">
              <label for="per_day_bottles_to_deliver">No. of Bottles (Per Day)</label>
              <input type="number" class="form-control" id="per_day_bottles_to_deliver" placeholder="Enter No. of bottles" name="per_day_bottles_to_deliver" value="<?= $data['per_day_bottles_to_deliver'] ?>" required>
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" rows="3" id="description" placeholder="Enter Description" name="description" required><?= $data['description'] ?></textarea>
            </div>

            <div>
              <img src="<?= $base_image_url ?><?= $data['photo_dir'] ?>tm_<?=  $data['photo'] ?>" />
            </div>

            <div class="form-group">
              <label for="description">Primary Photo:</label>
              <?php echo $this->Form->control('photo', ['type' => 'file','label'=>false]); ?>
            </div>

          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <button type="submit" class="btn btn-primary">Edit Subscription</button>
          </div>
          <!-- /.card-footer-->
        </div> <!-- Card Ends -->
        </form>
      </div>
    </div>
  </div>
</section>