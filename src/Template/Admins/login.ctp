<?= $this->Form->create('Admin'); ?>
  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Username" name="username" id="username">
    <div class="input-group-append">
      <span class="fa fa-envelope input-group-text"></span>
    </div>
  </div>
  <div class="input-group mb-3">
    <input type="password" class="form-control" name="password" placeholder="Password" id="password">
    <div class="input-group-append">
      <span class="fa fa-lock input-group-text"></span>
    </div>
  </div>
  <div class="row">
    <div class="col-8">
      <div class="checkbox icheck">
        <label>
          <input type="checkbox"> Remember Me
        </label>
      </div>
    </div>
    <!-- /.col -->
    <div class="col-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
    </div>
    <!-- /.col -->
  </div>
</form>