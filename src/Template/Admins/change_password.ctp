 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Change Password</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Admins</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-6">
      <?= $this->Form->create('Admin', ['role'=>'form']); ?>
        <div class="card card-primary">
        <!-- Header -->
          <div class="card-header">
            <h3 class="card-title">Change Password</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>

          <!-- Body -->
          <div class="card-body" style="display: block;">
            <div class="form-group">
              <label for="password">New Password</label>
              <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" required>
            </div>

            <div class="form-group">
              <label for="rpassword">Retype Password</label>
              <input type="password" class="form-control" id="rpassword" placeholder="Retype Password" name="rpassword" required>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <button type="submit" class="btn btn-primary">Change Password</button>
          </div>
          <!-- /.card-footer-->
        </div> <!-- Card Ends -->
        </form>
      </div>
    </div>
  </div>
</section>