<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Asgard Admin | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <!-- Dependencies CSS -->
  <?= $this->Html->css([
    'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
    'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    'adminlte.min',
    'blue',
    'flash',
    'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700'
  ])
  ?>

</head>

<body class="hold-transition login-page">
<?= $this->Flash->render() ?>

<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>ASGARD</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <?= $this->fetch('content') ?>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

  <!-- Scripts -->
  <?= $this->Html->script([
      'jquery.min',
      'bootstrap.bundle.min',
      'icheck.min'
    ]) 
  ?>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
  $(document).ready(function() {
    setTimeout(() => {
      $('.message').css('display','none');
    }, 5000);
  });
</script>
</body>
</html>
