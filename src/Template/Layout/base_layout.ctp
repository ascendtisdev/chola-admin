<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $page_title ?> | Asgard Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Dependencies CSS -->
  <?= $this->Html->css([
    'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
    'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    'adminlte.min',
    'blue',
    'flash',
    'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
    'asgard',
    'https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css'
  ])
  ?>

</head>



<body class="hold-transition sidebar-mini">
<?= $this->Flash->render() ?>

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-primary navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
     
      <?php //$this->element('messages') ?>
      <?php //$this->element('notifications') ?>
      
      <li class="nav-item dropdown">
      
        <a class="nav-link d-flex  justify-content-center align-items-center" data-toggle="dropdown" href="#">
          <div style="padding-right: 10px;" class="d-none d-md-block"><?= $user['name'] ?></div>
          <i class="fa fa-user"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

          <div class="dropdown-divider"></div>
          <a href="<?= $this->Url->build(["controller" => "admins", "action" => "change_password"]);?>" class="dropdown-item" style="color: black !important;">
            <i class="fa fa-lock mr-2"></i> Change Password
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?= $this->Url->build(["controller" => "admins", "action" => "logout"]);?>" class="dropdown-item" style="color: black !important;">
            <i class="fa fa-sign-out-alt mr-2"></i> Logout
          </a>
        </div>

      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fa fa-th-large"></i>
        </a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <?= $this->element('sidebar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?= $this->fetch('content') ?>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0
    </div>
    <strong>Copyright &copy; <?= date('Y') ?> <a href="https://ascendtis.com">Ascendtis</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- Scripts -->
<?= $this->Html->script([
    'jquery.min',
    'bootstrap.bundle.min',
    'icheck.min',
    'jquery.slimscroll.min',
    'fastclick.min',
    'adminlte.min',
    'demo',
    'material_datepicker.min',
    'app'
  ]) 
?>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 

    setTimeout(() => {
      $('.message').css('display','none');
    }, 4000);
  });
</script>


</body>
</html>