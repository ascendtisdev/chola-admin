 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Product</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Products</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-6">
      <?= $this->Form->create('Product', ['role'=>'form','type' => 'file']); ?>
        <div class="card card-primary">
        <!-- Header -->
          <div class="card-header">
            <h3 class="card-title">Edit Product</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>

          <!-- Body -->
          <div class="card-body" style="display: block;">
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?= $data['name'] ?>" required>
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" rows="3" id="description" placeholder="Enter Description" name="description" required><?= $data['description'] ?></textarea>
            </div>

            <div class="form-group">
              <label for="listed_price">Listed Price</label>
              <input type="number" class="form-control" id="listed_price" placeholder="Enter Price" name="listed_price" value="<?= $data['listed_price'] ?>" required>
            </div>

            <div class="form-group">
              <label for="discount_price">Discounted Price</label>
              <input type="number" class="form-control" id="discount_price" placeholder="Enter Discount Price" name="discount_price" value="<?= $data['discount_price'] ?>" required>
            </div>

            <div>
              <img src="<?= $base_image_url ?><?= $data['photo_dir'] ?>tm_<?=  $data['photo'] ?>" />
            </div>

            <div class="form-group">
              <label for="description">Primary Photo:</label>
              <?php echo $this->Form->control('photo', ['type' => 'file','label'=>false]); ?>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer" style="display: block;">
            <button type="submit" class="btn btn-primary">Edit Product</button>
          </div>
          <!-- /.card-footer-->
        </div> <!-- Card Ends -->
        </form>
      </div>
    </div>
  </div>
</section>