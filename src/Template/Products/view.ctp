 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Products</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Products</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <div class="container-fluid">

      <!-- Default box -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">View Products</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

        <!-- DataTable -->

        <div class="dataTables_wrapper dt-bootstrap4">

          <div class="row">
            <div class="col-sm-12">
              <table id="main-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Listed Price (₹)</th>
                  <th>Discounted Price (₹)</th>
                  <th>CreatedAt</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>

              <?php foreach ($data as $key=>$val): ?>
                <tr>
                  <td class="table-image-view">
                    <img src="<?= $base_image_url ?><?= $val['photo_dir'] ?>tm_<?=  $val['photo'] ?>" />
                  </td>
                  <td><?= $val['name'] ?></td>
                  <td><h4><span class="badge badge-success"><?= $val['listed_price'] ?></span></h4></td>
                  <td><h4><span class="badge badge-success"><?= $val['discount_price'] ?></span></h4></td>
                  <td><?= date('jS F Y',strtotime( $val['created_at']) ) ?></td>
                  <td class="action-btn-container">
                    <a href="<?= $this->Url->build(["controller" => "products", "action" => "edit", $val['id']]);?>" class="btn btn-sm btn-info action-btn" data-toggle="tooltip" title="Edit Product"><i class="fas fa-pencil-alt"></i></a>
                    <a href="<?= $this->Url->build(["controller" => "products", "action" => "delete", $val['id']]);?>" class="btn btn-sm btn-danger action-btn" onclick="return confirm('Are you sure? This action wont be rollback.')" data-toggle="tooltip" title="Delete Product"><i class="fas fa-times"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
               
              </tbody>
              <tfoot>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Listed Price (₹)</th>
                  <th>Discounted Price (₹)</th>
                  <th>CreatedAt</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
   
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info">
                <?= $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries') ?>
              </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers d-flex justify-content-end">
                <ul class="pagination">
                  <?php
                    echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                  ?>
                </ul>
              </div>
            </div>
          </div>

        </div>
        <!-- DataTable Ends Here -->

        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    <!-- FAB -->
    <div class="floating-btn">
      <a href="<?= $this->Url->build(["controller" => "products", "action" => "add"]);?>" class="btn btn-sm btn-primary floating-action-btn" data-toggle="tooltip" title="Add Product">
        <i class="fa fa-plus"></i>
      </a>
    </div>
    <!-- FAB ENDS -->
  </div>
</section>