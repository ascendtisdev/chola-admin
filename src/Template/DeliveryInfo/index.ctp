 <!-- Content Header (Page header) -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Delivery Info</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Delivery Info</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->

  
<section class="content">
  <div class="container-fluid">

      <!-- Default box -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">View Delivery Info</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
  
      <!-- Filters -->
      <?= $this->Form->create('DeliveryInfo', ['type'=>'GET','id'=>'filters','role'=>'form']); ?>
      <div class="row">
    
        <div class="col-sm-12">
          <h5>Filters:</h5>
        </div>
        <div class="col-sm-3">

          <div class="form-group">
            <label for="statusDelivery">Status:</label>
            <select class="form-control" id="statusDelivery" name="status" value="<?= $filter_status ?>" onChange="onFilterChange()">
              <option value="ALL"  <?= ($filter_status == 'ALL') ? "selected" : "" ?>>All</option>
              <option value="PENDING"  <?= ($filter_status == 'PENDING') ? "selected" : "" ?>>Pending</option>
              <option value="DELIVERED"  <?= ($filter_status == 'DELIVERED') ? "selected" : "" ?>>Delivered</option>
              <option value="NOT_DELIVERED"  <?= ($filter_status == 'NOT_DELIVERED') ? "selected" : "" ?>>Not Delivered</option>
              <option value="NOT_AVAILABLE"  <?= ($filter_status == 'NOT_AVAILABLE') ? "selected" : "" ?>>Not Available</option>
            </select>
          </div>

        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label for="start_date">From Date:</label>
            <input id="start_date" class="form-control" value="<?= $filter_start_date ?>" onChange="onStartDateChange()" name="start_date"/>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group">
            <label for="end_date">To Date:</label>
            <input id="end_date" class="form-control" value="<?= $filter_end_date ?>" onChange="onEndDateChange()" name="end_date"/>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="form-group">
            <label for="end_date">Assign To:</label>
            <?= $this->Form->select('assign_to', $runners_list,[
                'id' => 'assign_to',
                'empty' => '(Choose Delivery Boy)',
                'class' => 'form-control',
                'onChange' => 'onAssignChange()'
              ])  
            ?>
          </div>
        </div>

      </div>
      </form>

      <!-- DataTable -->


        <div class="dataTables_wrapper dt-bootstrap4">

          <?= $this->Form->create(null, ['type'=>'POST','id'=>'data_tables','role'=>'form','url'=>['controller'=>'delivery_info','action'=>'assign']]); ?>
            <input type="hidden" name="assign_to" id="hidden_assign_to" />
            <div class="row">
              <div class="col-sm-12">
                <table id="main-table" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>
                      <div class="checkbox icheck">
                        <label>
                          <input type="checkbox" id="checkAll" class="checkAll" />
                        </label>
                      </div>
                    </th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Qty. (ML)</th>
                    <th>User</th>
                    <th>Delivery Slot</th>
                    <th>Delivery<br/>AssignTo</th>
                    <th>AddOn Qty. (ML)</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $key=>$val): ?>
                  <tr>
                    <td>
                      <div class="checkbox icheck">
                        <label>
                          <!-- <input type="checkbox" name=""> -->
                          <?= $this->Form->checkbox('check[]', ['value' => $val['id'],'hiddenField' => false,'class'=>'check_1']) ?>
                        </label>
                      </div>

                    </td>
                    <td>
                      <h4><?= date('dS', strtotime($val['date'])) ?></h4>
                      <h5><?= date('F, Y', strtotime($val['date'])) ?></h5>
                      <h4><span class="badge badge-info"><?= date('l', strtotime($val['date'])) ?></span></h4>
                    </td>
                    <td>
                      <?php if ($val['status'] === 'PENDING'): ?>
                        <h4><span class="badge badge-secondary">Pending</span></h4>
                      <?php elseif ($val['status'] === 'DELIVERED'): ?>
                        <h4><span class="badge badge-success">Delivered</span></h4>
                      <?php elseif ($val['status'] === 'NOT_DELIVERED'): ?>
                        <h4><span class="badge badge-danger">Not Delivered</span></h4>
                      <?php elseif ($val['status'] === 'NOT_AVAILABLE'): ?>
                        <h4><span class="badge badge-warning">Not Available</span></h4>
                      <?php endif; ?>
                    </td>
                    <td>
                      <h4>
                        <span class="badge badge-info"><?= $val['qty'] ?></span>
                      </h4>
                    </td>
                    <td>
                      <?= $val['user']['name'] ?>
                    </td>
                    <td>
                      <?php if ($val['delivery_slot'] === 'MORNING'): ?>
                        <h4><span class="badge badge-info">Morning</span></h4>
                      <?php elseif ($val['delivery_slot'] === 'EVENING'): ?>
                        <h4><span class="badge badge-warning">Evening</span></h4>
                      <?php endif; ?>
                    </td>
                    <td>
                      <?php if ($val['runner']['user']['name']): ?>
                        <h6><?= $val['runner']['user']['name'] ?></h6>
                      <?php endif; ?>
                    </td>
                    <td>
                      <h4>
                        <span class="badge badge-info"><?= $val['addon_qty'] ?></span>
                      </h4>
                    </td>
                    <td class="action-btn-container">
                        <button type="button" class="btn btn-info btn-sm action-btn" data-toggle="modal" data-target="#editDeliveryInfo<?= $val['id'] ?>" data-toggle="tooltip" title="Edit Delivery Info"><i class="fas fa-pencil-alt"></i></button>
                      <!-- <a href="<?= $this->Url->build(["controller" => "delivery_info", "action" => "delete", $val['id']]);?>" class="btn btn-sm btn-danger action-btn" onclick="return confirm('Are you sure? This action wont be rollback.')" data-toggle="tooltip" title="Delete User"><i class="fas fa-times"></i></a> -->
                    </td>
                  </tr>
                <?php endforeach; ?>
                
                </tbody>
                <tfoot>
                  <tr>
                    <th>
                      <div class="checkbox icheck">
                        <label>
                          <input type="checkbox" id="checkAll" class="checkAll" />
                        </label>
                      </div>
                    </th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Qty. (ML)</th>
                    <th>User</th>
                    <th>Delivery Slot</th>
                    <th>Delivery<br/>AssignTo</th>
                    <th>AddOn Qty. (ML)</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
              </table>
              </div>
            </div>
          </form>
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info">
                <?= $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries') ?>
              </div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers d-flex justify-content-end">
                <ul class="pagination">
                  <?php
                    echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                  ?>
                </ul>
              </div>
            </div>
          </div>

          </div>
          <!-- DataTable Ends Here -->

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!--  -->
      <?php foreach ($data as $key=>$val): ?>
        <?= $this->element('DeliveryInfo/EditDeliveryInfoModal',['data' => $val]) ?>
      <?php endforeach; ?>
  </div>
</section>

<script>
  let old_start_date = document.getElementById('start_date').value;
  let old_end_date = document.getElementById('end_date').value;

  function onStartDateChange() {
    const date = document.getElementById('start_date').value;
    if (date != old_start_date) {
      //Submit Form
      applyFilters();
    }
  }
  function onEndDateChange() {
    const date = document.getElementById('end_date').value;
    if (date != old_end_date) {
      //Submit Form
      applyFilters();
    }
  }
  function onFilterChange() {
    const status = document.getElementById('statusDelivery').value;
  
    applyFilters();
    console.log("STATUS: ", status);
    console.log("Date: ", date);
  }

  function applyFilters() {
    let form = document.getElementById('filters');
    form.submit();
  }

  function onAssignChange() {
    let assignTo = document.getElementById('assign_to').value;
    
    document.getElementById('hidden_assign_to').value = assignTo;

    let dataTableForm = document.getElementById('data_tables');
    if (assignTo !== '') {
      dataTableForm.submit();
    }
    console.log("onAssignChange: ", assignTo);
    
  }

  // function onSelectAll() {
  //   console.log("[*] onSelectAll");
  //   const checkboxes = document.getElementsByName('check[]');
  //   for(var i=0, n=checkboxes.length;i<n;i++) {
  //     checkboxes[i].checked = true;
  //   }

  // console.log("CheckBoxes: ",  checkboxes);
  // }
</script>