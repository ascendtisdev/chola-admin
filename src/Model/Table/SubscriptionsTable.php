<?php
namespace App\Model\Table;

use Cake\Utility\Security;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\EventManager;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use ArrayObject;

/**
*   Subscriptions Model
*
*/
class SubscriptionsTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
      parent::initialize($config);

      $this->setTable('subscriptions');
      $this->setPrimaryKey('id');

      $this->addBehavior('Timestamp', [
        'events' => [
          'Model.beforeSave' => [
            'created_at' => 'new',
            'updated_at' => 'always'
          ]
        ]
      ]);
          
      $client = \Aws\S3\S3Client::factory([
        'endpoint' => getenv('END_POINT'),
          'credentials' => [
            'key'    => getenv('ACCESS_KEY'),
            'secret' => getenv('SECRET_KEY'),
          ],
          'region' => getenv('REGION'),
          'version' => 'latest',
          'http' => [
            'verify' => false
          ]
      ]);
      $adapter = new \League\Flysystem\AwsS3v3\AwsS3Adapter(
          $client,
          getenv('BUCKET_NAME'),
          ''
      );
    
      /**
       * Upload Plugin Config
       */
      $this->addBehavior('Josegonzalez/Upload.Upload', [
          // You can configure as many upload fields as possible,
          // where the pattern is `field` => `config`
          //
          // Keep in mind that while this plugin does not have any limits in terms of
          // number of files uploaded per request, you should keep this down in order
          // to decrease the ability of your users to block other requests.
          'photo' => [
            // 'path' => 'webroot{DS}files{DS}{model}{DS}{field}{DS}{field-value:id}{DS}',
            'fields' => [
              'dir' => 'photo_dir',
              'size' => 'photo_size',
              'type' => 'photo_type'
            ],
            'nameCallback' => function ($table, $entity, $data, $field, $settings) {
              return strtolower($data['name']);
            },
            // our S3 adapter
            'filesystem' => [
              'adapter' => $adapter,
            ],
            'transformer' =>  function ($table, $entity, $data, $field, $settings) {
              $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

              // Store the thumbnail in a temporary file
              $tm_tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
              $sm_tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
              $md_tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

              // Use the Imagine library to DO THE THING
              $tm_size = new \Imagine\Image\Box(100, 100);
              $sm_size = new \Imagine\Image\Box(320, 320);
              $md_size = new \Imagine\Image\Box(600, 600);

              $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
              $imagine = new \Imagine\Gd\Imagine();

              // Save that modified file to our temp file
              //Thumbnail
              $imagine->open($data['tmp_name'])
                  ->thumbnail($tm_size, $mode)
                  ->save($tm_tmp);
              //Small
              $imagine->open($data['tmp_name'])
                  ->thumbnail($sm_size, $mode)
                  ->save($sm_tmp);

              //Medium
              $imagine->open($data['tmp_name'])
                  ->thumbnail($md_size, $mode)
                  ->save($md_tmp);


              // Now return the original *and* the thumbnail
              return [
                  $data['tmp_name'] => $data['name'],
                  $tm_tmp => 'tm_' . $data['name'],
                  $sm_tmp => 'sm_' . $data['name'],
                  $md_tmp => 'md_' . $data['name']
              ];
            },
            'deleteCallback' => function ($path, $entity, $field, $settings) {
                // When deleting the entity, both the original and the thumbnail will be removed
                // when keepFilesOnDelete is set to false
                return [
                    $path . $entity->{$field},
                    $path . 'tm_' . $entity->{$field},
                    $path . 'sm_' . $entity->{$field},
                    $path . 'md_' . $entity->{$field}
                ];
            },
            'keepFilesOnDelete' => false
          ]
      ]);

      //Relations goes here....
      //which is hasMany, belongsTo & many more
  }
}
?>