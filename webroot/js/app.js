$(document).ready(function() {
  const start_date = $('#start_date').datepicker({
    showOtherMonths: true,
    format: 'yyyy-mm-dd'
  });
  const end_date = $('#end_date').datepicker({
    showOtherMonths: true,
    format: 'yyyy-mm-dd'
  });
  $( "#start_date" ).click(function() {
    start_date.open();
  });
  
  $( "#end_date" ).click(function() {
    end_date.open();
  });
  
  $('.checkAll').on('ifUnchecked', function(event){
    // alert(event.type + 'Unchecked callback');
    $('.check_1').iCheck('toggle');
  });

  $('.checkAll').on('ifChecked', function(event){
    // alert(event.type + ' callback');
    $('.check_1').iCheck('toggle');
  });
  
  
  
});
